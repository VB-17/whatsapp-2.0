import { Avatar, IconButton } from "@material-ui/core";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import ChatIcon from "@material-ui/icons/Chat";
import SearchIcon from "@material-ui/icons/Search";
import styled from "styled-components";

function Sidebar() {
  return (
    <Container>
      <Header>
        <UserAvatar />
        <IconsContainer>
          <IconButton>
            <ChatIcon />
          </IconButton>
          <IconButton>
            <MoreVertIcon />
          </IconButton>
        </IconsContainer>
      </Header>
      <Search>
        <SearchIcon />
        <SearchInput placeholder="Search in Chat" />
      </Search>
      <SidebarButton>Start a new chat</SidebarButton>

      {/* List of Chats */}
    </Container>
  );
}

export default Sidebar;

const Container = styled.div``;

const Header = styled.div`
  position: sticky;
  background-color: white;
  top: 0;
  z-index: 1;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 15px;
  height: 80px;
  border-bottom: 1px solid whitesmoke;
`;

const UserAvatar = styled(Avatar)`
  cursor: pointer;
  :hover {
    opacity: 0.8;
  }
`;

const IconsContainer = styled.div``;

const Search = styled.div`
  display: flex;
  align-items: flex-start;
  border-radius: 2px;
`;

const SearchInput = styled.input`
  outline-width: 0;
  border: none;
  flex: 1;
`;

const SidebarButton = styled.button`
  width: 100%;
  padding: 10px;
  cursor: pointer;
  border-top: 1px solid whitesmoke;
  border-bottom: 1px solid whitesmoke;

  font-size: 16px;

  :hover {
    opacity: 0.8;
  }
`;
