import firebase from "firebase";

const firebaseConfig = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  projectId: process.env.PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID,
};

// Prevents the app from intializing multiple times
const app = !firebase.apps.length
? firebase.initializeApp(firebaseConfig)
  : firebase.app();

const db = app.firebase();
const auth = app.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export {db , auth , provider}